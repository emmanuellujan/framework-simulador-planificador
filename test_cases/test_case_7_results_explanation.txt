	En el siguiente texto se explicarán los primeros 10 tiempos del caso de estudio 7 en pos de entender la forma de leer los reportes en txt de los demás casos. El lector comprobará que la interpretación de los resultados es intuitiva. En este ejemplo en particular se tienen dos procesos que se ejecutarán sobre dos dispositivos. Cada proceso se ejecuta primero en el dispositivo 0 una vez y luego tres veces en el dispositivo uno. Esta secuencia se repite cuatro veces.

	Primero se cargan todos los procesos a la lista de listos del dispositivo deliverDev, quien se encargará de irlos repartiendo a los dispositivos correspondientes. Los procesos se repartirán por orden de llegada, o sea siguiendo el algoritmo FCFS.

Tiempo: 0

	En este tiempo deliverDev selecciona al proceso 0 de la lista de listos con el algoritmo FCFS y lo pondrá como activo.
	Los demás dispositivos no hacen nada ya que no tienen procesos que ejecutar.

Tiempo: 1

	El dispositivo deliverDev leerá dónde debe ejecutarse primero el proceso 0. El proceso debe ejecutarse en el dispositivo 0, entonces éste es pasado a a la lista de listos de dicho dispositivo. El pasaje se concreta al final del tiempo 1, esto es debido a que los dispositivos se ejecutan en paralelo y concretan sus acciones al final de cada tiempo. Esta medida se toma para lograr sincronismo.
	Los demás dispositivos no hacen nada ya que no tienen procesos que ejecutar.

Tiempo: 2
	
	A partir de ahora el dispositivo deliverDev vuelve a repetir las acciones realizadas en los tiempos 0 y 1 para cada proceso que deba repartir hasta agotarlos. En este tiempo selecciona al proceso 1 de su lista de listos y lo pone como activo.
	El dispositivo 0, que ahora sí tiene un proceso en su lista de listos, ejecuta su algoritmo de planificación, lo saca de la lista de listos, y lo pone como activo.
	El dispositivo 1 no tiene acciones que realizar.
	

Tiempo: 3
	
	El dispositivo deliverDev pasa el proceso que eligió en el tiempo anterior al dispositivo dónde debe ejecutarse, el dispositivo 0.
	El dispositivo 0 ejecuta al proceso 0.
	El dispositivo 0 no tiene acciones que realizar.

Tiempo: 4
	
	El dispositivo deliverDev selecciona su último proceso de su lista de listos y lo pone como activo.
	El dispositivo 0 ya no debe seguir ejecutando al proceso 0, éste debe pasar a la lista de listos del dispositivo 1.
	El dispositivo 0 no tiene acciones que realizar. Al final de este tiempo el proceso 0 llega a la lista de listos, estará disponible para su uso en el tiempo siguiete.

Tiempo: 5
	
	El dispositivo deliverDev pasa su último proceso a la lista de interrupciones del dispositivo 0. Un proceso puede necesitar ejecutarse normalmente en un dispositivo, en cuyo caso se lo pasa a la lista de listos; o bien puede necesitar interrumpir a un dispositivo, en este caso se lo pasa a la lista de interrupciones de dicho dispositivo.
	Al dispositivo 0 se le ha vencido el temporizador. En este caso sólo se debe reiniciar el temporizador, pero si hubiese tenido un proceso activo lo hubiera desalojado, excepto que fuera una interrupción.
	El dispositivo 1 selecciona al proceso 0 de su lista de listos y lo pasa a estado activo.

Tiempo: 6

	A partir de ahora el dispositivo deliverDev no realizará acciones.
	El dispositivo 0 verifica que su lista de interrupciones y selecciona al proceso 2 de ella (que es el proceso que está interrumpiendo) y lo pasa a estado activo.
	El dispositivo 1 ejecuta al proceso 0. Este proceso fue puesto como activo en el tiempo anterior.

Tiempo: 7
	
	El dispositivo 0 ejecuta al proceso 2.
	El dispositivo 1 ejecuta al proceso 0.

Tiempo: 8
	
	El dispositivo 0 pasa su proceso activo a la lista de listos del dispositivo 1 ya que este proceso debe ejecutarse ahora ahí.
	El dispositivo 1 continua ejecutando al proceso 0.

Tiempo: 9
	
	El dispositivo 0 selecciona al único elemento, el proceso 1, en su lista de listos y lo pone como activo.
	El dispositivo 1 pasa el proceso 0 a la lista de listos del dispositivo 0. Debido a que el pasaje se realiza en este tiempo el dispositivo 0 no puede disponer del proceso 0 hasta el próximo tiempo. 

Tiempo: 10

	Al dispositivo 0 se le ha vencido el temporizador. El proceso activo, proceso 1, es desalojado y pasa a su lista de listos.
	El dispositivo 1, que no tenía proceso activo, selecciona al proceso 2 de su lista de listos y lo pone como activo.

































