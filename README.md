Este proyecto fue realizado para la materia Taller de Tiempo Real para Control Robótico de la carrera Ingeniería de Sistemas,
en la Universidad Nacional del Centro de la Provincia de Buenos Aires, durante Noviembre de 2010.

El objetivo fue desarrollar un framework de simulación de planificación de procesos sobre los dispositivos de un computador.
